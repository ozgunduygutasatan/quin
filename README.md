# Quin QA Automation Assignment

# Technologies
I used within this solution including:

- Java
- Maven
- RestAssured
- Cucumber
- JUnit

## How To Run
You can run this tests by running the following command:

```
mvn test
```

