Feature: Read Bin

  Scenario: I should be able to read a bin with bin Id
    Given I have a bin already created
      """
      {"name":"test-record"}
      """
    When I send the binId
    Then response status code should be 200
    And I should see "test-record"

  Scenario: Read Bin with null X-Master-Key header
    Given I have a bin already created
      """
      {"name":"test-record"}
      """
    When I send the binId with null X-Master-Key header
    Then response status code should be 401

  Scenario: Read Not Existed Bin
    Given I have a bin already created
      """
      {"name":"test-record"}
      """
    When I send the binId which is not existed
    Then response status code should be 422