Feature: Update Bin

  Scenario: I should be able to update existed bin
    Given I have a bin already created
      """
      {"name":"test-record"}
      """
    When I send the binId to update
      """
      {"name":"test-record-updated"}
      """
    Then response status code should be 200
    And I should see "test-record-updated"

  Scenario: I should be able to update existed bin
    Given I have a bin already created
      """
      {"name":"test-record"}
      """
    When I send not existed binId "60881d63be95aad0cc0a0433" to update
      """
      {"name":"test-record-updated"}
      """
    Then response status code should be 404

  Scenario: Update Bin with null X-Master-Key header
    Given I have a bin already created
      """
      {"name":"test-record"}
      """
    When I send the binId to update with null X-Master-Key header
      """
      {"name":"test-record-updated"}
      """
    Then response status code should be 401