Feature: Delete Bin

  Scenario: I should be able to delete existed bin
    Given I have a bin already created
      """
      {"name":"test-record"}
      """
    When I send the binId to delete
    Then response status code should be 200
    When I request the bin again
    Then response status code should be 404

  Scenario: Delete Bin with null X-Master-Key header
    Given I have a bin already created
      """
      {"name":"test-record"}
      """
    When I send the binId to delete with null X-Master-Key header
    Then response status code should be 401