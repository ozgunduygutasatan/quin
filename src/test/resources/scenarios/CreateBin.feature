Feature: Create Bin

  Scenario: I should be able to create a new bin
    Given create bins endpoint exists
    When I send a valid create bin payload
      """
      {"name":"test-record"}
      """
    Then response status code should be 200
    And I should see "test-record"

  Scenario: Create Bin with null content-type header
    Given create bins endpoint exists
    When I send create bin payload with null content-type header
      """
      {"name":"test-record"}
      """
    Then response status code should be 400

  Scenario: Create Bin with null payload
    Given create bins endpoint exists
    When I send null create bin payload
    Then response status code should be 400

  Scenario: Create Bin with 129 char long X-Bin-Name
    Given create bins endpoint exists
    When I send create bin payload with 129 char long X-Bin-Name
      """
      {"name":"test-record"}
      """
    Then response status code should be 400

  Scenario: Create Bin with null X-Master-Key header
    Given create bins endpoint exists
    When I send create bin payload with null X-Master-Key header
      """
      {"name":"test-record"}
      """
    Then response status code should be 401
