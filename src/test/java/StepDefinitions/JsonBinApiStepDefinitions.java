package StepDefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.Assert;
import org.junit.Before;

import static io.restassured.RestAssured.given;

public class JsonBinApiStepDefinitions {

    private static final String BASE_URI = "https://api.jsonbin.io/v3";
    private static final String API_PATH = "/b";
    private static final String API_MASTER_KEY = "$2b$10$qVnelnz40aqqdFGbADM9GuAWBeNeEf2ftL/fDzYw3gXNmmCUv4ucq";

    private Response response;
    private String binId;

    //************************************************************************************************************
    //CREATE DEFINITIONS
    //************************************************************************************************************

    @Given("create bins endpoint exists")
    public void preRequest() {
        RestAssured.baseURI = BASE_URI;
    }

    @When("I send a valid create bin payload")
    public void sendCreateBinRequest(String payload) {
        response = given()
                .header("X-Master-Key", API_MASTER_KEY)
                .contentType(ContentType.JSON)
                .body(payload)
                .post(API_PATH)
                .then()
                .extract()
                .response();

        binId = response.jsonPath().get("metadata.id");
    }

    @When("I send create bin payload with null content-type header")
    public void sendCreateBinPayloadWithNullContentType(String payload) {
        response = given()
                .header("X-Master-Key", API_MASTER_KEY)
                .body(payload)
                .post(API_PATH)
                .then()
                .extract()
                .response();
    }

    @When("I send null create bin payload")
    public void sendCreateBinNullPayload() {
        response = given()
                .header("X-Master-Key", API_MASTER_KEY)
                .contentType(ContentType.JSON)
                .body("")
                .post(API_PATH)
                .then()
                .extract()
                .response();
    }

    @When("I send create bin payload with 129 char long X-Bin-Name")
    public void sendCreateBinWith129LongBinName(String payload) {
        response = given()
                .header("X-Master-Key", API_MASTER_KEY)
                .header("X-Bin-Name", "7zwAFGHsH9oA5jQ10eoIYbRnRT5LqHj7CBuI6oJ1p0YpnLgaygXLvO7pVgkFFLDVVHtWn69usKhjYF4iG1ylQEu1kbD93SBKELx2bSNT9pt6G9E9VugEWk58o8xNqTNCZ")
                .contentType(ContentType.JSON)
                .body(payload)
                .post(API_PATH)
                .then()
                .extract()
                .response();
    }

    @When("I send create bin payload with null X-Master-Key header")
    public void sendCreateBinWithNullXMasterKey(String payload) {
        response = given()
                .contentType(ContentType.JSON)
                .body(payload)
                .post(API_PATH)
                .then().extract().response();
    }

    @Then("response status code should be {int}")
    public void checkResponseStatusCode(int expectedStatusCode) {
        int actualStatusCode = response.getStatusCode();
        Assert.assertEquals(expectedStatusCode, actualStatusCode);
    }

    @And("I should see {string}")
    public void verifyResponse(String expectedRecordName) {
        String actualRecordName = response.jsonPath().get("record.name");
        Assert.assertEquals(expectedRecordName, actualRecordName);
    }


    //************************************************************************************************************
    //READ DEFINITIONS
    //************************************************************************************************************

    @Given("I have a bin already created")
    public void createBin(String payload) {
        RestAssured.baseURI = BASE_URI;
        sendCreateBinRequest(payload);
    }

    @When("I send the binId")
    public void sendBinId() {
        response = given()
                .header("X-Master-Key", API_MASTER_KEY)
                .contentType(ContentType.JSON)
                .get(String.format("%s/%s/latest", API_PATH, binId))
                .then().extract().response();
    }

    @When("I send the binId which is not existed")
    public void sendNotExistedBinId() {
        response = given()
                .header("X-Master-Key", API_MASTER_KEY)
                .contentType(ContentType.JSON)
                .get(String.format("%s/%s/latest", API_PATH, "not-existed-binId"))
                .then().extract().response();
    }

    @When("I send the binId with null X-Master-Key header")
    public void sendBinIdWithNullXMasterKey() {
        response = given()
                .contentType(ContentType.JSON)
                .get(String.format("%s/%s/latest", API_PATH, binId))
                .then().extract().response();
    }

    //************************************************************************************************************
    //DELETE DEFINITIONS
    //************************************************************************************************************

    @When("I send the binId to delete")
    public void sendBinIdToDelete() {
        response = given()
                .header("X-Master-Key", API_MASTER_KEY)
                .delete(String.format("%s/%s", API_PATH, binId))
                .then().extract().response();
    }

    @When("I request the bin again")
    public void getBin() {
        response = given()
                .header("X-Master-Key", API_MASTER_KEY)
                .get(String.format("%s/%s/latest", API_PATH, binId))
                .then().extract().response();
    }

    @When("I send the binId to delete with null X-Master-Key header")
    public void sendBinIdToDeleteWithNullXMasterKey() {
        response = given()
                .delete(String.format("%s/%s", API_PATH, binId))
                .then().extract().response();
    }

    //************************************************************************************************************
    //UPDATE DEFINITIONS
    //************************************************************************************************************

    @When("I send the binId to update")
    public void sendBinIdToUpdate(String payload) {
        response = given()
                .contentType(ContentType.JSON)
                .header("X-Master-Key", API_MASTER_KEY)
                .body(payload)
                .put(String.format("%s/%s", API_PATH, binId))
                .then().extract().response();
    }

    @When("I send the binId to update with null X-Master-Key header")
    public void sendBinIdToUpdateWithNullXMasterKey(String payload) {
        response = given()
                .contentType(ContentType.JSON)
                .body(payload)
                .put(String.format("%s/%s", API_PATH, binId))
                .then().extract().response();
    }

    @When("I send not existed binId {string} to update")
    public void sendInvalidBinIdToUpdate(String invalidBinId, String payload) {
        response = given()
                .contentType(ContentType.JSON)
                .body(payload)
                .put(String.format("%s/%s", API_PATH, invalidBinId))
                .then().extract().response();
    }
}
